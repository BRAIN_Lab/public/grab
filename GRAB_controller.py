#!/usr/bin/env python

# *******************************************
# *                                         *
# *            GRAB controller              *
# *                                         *
# *******************************************
# Author: Sujet Phodapol
# Contact: sujet.ph@gmail.com,
# Date: 01/04/2021
# Paper: GRAB:  GRAdient-Based  Shape-Adaptive  Compliant  LocomotionControl

#===== Robot Configuration (MORF)    
#
#                              //===========
#            (FT0)             ||
#           //   \\            ||
#          //     \\           || Robot Body
# Leg tip ()       (CF0)==(BC0)||
#                              ||
# 
#                     //=====\\
#              /\    ||       ||    /\
#       Leg 0 /  \---|| () () ||---/  \ Leg 3
#                    ||       ||
#              /\    ||       ||    /\
#       Leg 1 /  \---||       ||---/  \ Leg 4
#                    ||       ||
#              /\    ||       ||    /\
#       Leg 2 /  \---||       ||---/  \ Leg 5
#                    ||       ||
#                     \\=====//

import rospy
import math
from std_msgs.msg import String
from std_msgs.msg import Float32
from std_msgs.msg import Float32MultiArray
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Imu
import csv
import time
import matplotlib.pyplot as plt
from itertools import izip_longest as zip_longest

#===== Define variables ===== 
pos_data = []
out1_data = []
out2_data = []
time_pos = []
time_data = []
imu_x = []
imu_y = []
imu_z = []
imu_w = []
time_imu = []
torque_data = []
time_torque = []
error1 = []
error2 = []
current = []
time_current = []
current_pos = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

#===== Define function ===== 
def rad2deg(rad):
    return rad*180.0/math.pi 

def deg2rad(deg):
    return deg/180.0*math.pi 

def pos_callback(data):
    global current_pos
    pos_data.append(data.data)
    time_pos.append(time.time())
    current_pos = data.data

def imu_callback(data):
    imu_x.append(data.orientation.x)
    imu_y.append(data.orientation.y)
    imu_z.append(data.orientation.z)
    imu_w.append(data.orientation.w)
    time_imu.append(time.time())

def torque_callback(data):
    torque_data.append(data.data)
    time_torque.append(time.time())

def current_callback(data):
    current.append(data.data)
    time_current.append(time.time())

def motor_command():
    rospy.init_node('motor_command', anonymous=True)
    rate = rospy.Rate(30)
    rospy.Subscriber('/joint_positions', Float32MultiArray, pos_callback)
    rospy.Subscriber('/imu', Imu, imu_callback)
    rospy.Subscriber('/joint_torques', Float32MultiArray, torque_callback)
    rospy.Subscriber('/current', Float32, current_callback)
    pub = rospy.Publisher('/multi_joint_command', Float32MultiArray, queue_size=10)

    #===== GRAB parameters ===== 
    phi = math.pi/12
    alpha = 1.4
    gamma1 = 2.0
    gamma2 = 2.0

    #===== post-process function parameters ===== 
    scale0 = 0.3
    scale1 = 0.15
    scale2 = 0.0

    #===== weight parameters ===== 
    w11 = alpha * math.cos(phi)
    w12 = alpha * math.sin(phi)
    w21 = -1 * alpha * math.sin(phi)
    w22 = alpha * math.cos(phi)

    #===== Initialization =====
    o1 = 0.01
    o2 = 0
    time.sleep(2)
    BC_cen = 0
    global CF_cen
    global FT_cen 
    CF_cen = 150
    FT_cen = -15
    motor_pos = Float32MultiArray()
    motor_pos.data   = [11,BC_cen, 12, deg2rad(CF_cen), 13,deg2rad(FT_cen),\
                        21,BC_cen, 22, deg2rad(CF_cen), 23,deg2rad(FT_cen),\
                        31,BC_cen, 32, deg2rad(CF_cen), 33,deg2rad(FT_cen),\
                        41,BC_cen, 42, deg2rad(CF_cen), 43,deg2rad(FT_cen),\
                        51,BC_cen, 52, deg2rad(CF_cen), 53,deg2rad(FT_cen),\
                        61,BC_cen, 62, deg2rad(CF_cen), 63,deg2rad(FT_cen)]
    pub.publish(motor_pos)
    time.sleep(2)
    print("======START======")

    while not rospy.is_shutdown():

        #===== CPG dynamic =====
        a1 = w11 * o1 + w12 * o2 
        a2 = w22 * o2 + w21 * o1

        o1 = math.tanh(a1)
        o2 = math.tanh(a2)

        #===== post-process function =====
        out1 = o1*scale0
        out2 = o2*scale1

        #===== GRAB dynamic =====
        # update with gradient
        o1 = o1 - gamma1 * (out1 - (-current_pos[0]))
        o2 = o2 - gamma2 * (out2 - (current_pos[1]-deg2rad(CF_cen)))   

        #===== Read sensors =====
        error1.append(out1 - (-current_pos[0]))
        error2.append(out2 - (current_pos[1]-deg2rad(CF_cen)))
        out1_data.append(rad2deg(-out1))
        out2_data.append(rad2deg(out2))
        time_data.append(time.time())        
        motor_pos.data   = [11,-out1,  12,  out2+deg2rad(CF_cen),  13,-out2*scale2 + deg2rad(FT_cen),\
                            21, out1,  22, -out2+deg2rad(CF_cen),  23, out2*scale2 + deg2rad(FT_cen),\
                            31,-out1,  32,  out2+deg2rad(CF_cen),  33,-out2*scale2 + deg2rad(FT_cen),\
                            41,-out1,  42, -out2+deg2rad(CF_cen),  43, out2*scale2 + deg2rad(FT_cen),\
                            51, out1,  52,  out2+deg2rad(CF_cen),  53,-out2*scale2 + deg2rad(FT_cen),\
                            61,-out1,  62, -out2+deg2rad(CF_cen),  63, out2*scale2 + deg2rad(FT_cen)]

        pub.publish(motor_pos)
        rate.sleep()


if __name__ == '__main__':
    #===== Send motor command =====
    motor_command()

    #===== Export data =====
    d = [time_data, out1_data, out2_data, error1, error2, time_imu, imu_x, imu_y, imu_z, imu_w,\
        time_pos, \
        [rad2deg(i[0]) for i in pos_data],  [rad2deg(i[1]) - CF_cen for i in pos_data],  [rad2deg(i[2]) - FT_cen for i in pos_data],\
        [rad2deg(i[3]) for i in pos_data],  [rad2deg(i[4]) - CF_cen for i in pos_data],  [rad2deg(i[5]) - FT_cen for i in pos_data],\
        [rad2deg(i[6]) for i in pos_data],  [rad2deg(i[7]) - CF_cen for i in pos_data],  [rad2deg(i[8]) - FT_cen for i in pos_data],\
        [rad2deg(i[9]) for i in pos_data],  [rad2deg(i[10]) - CF_cen for i in pos_data], [rad2deg(i[11]) - FT_cen for i in pos_data],\
        [rad2deg(i[12]) for i in pos_data], [rad2deg(i[13]) - CF_cen for i in pos_data], [rad2deg(i[14]) - FT_cen for i in pos_data],\
        [rad2deg(i[15]) for i in pos_data], [rad2deg(i[16]) - CF_cen for i in pos_data], [rad2deg(i[17]) - FT_cen for i in pos_data],\
        time_torque,\
        [i[0] for i in torque_data], [i[1] for i in torque_data], [i[2] for i in torque_data],\
        [i[3] for i in torque_data], [i[4] for i in torque_data], [i[5] for i in torque_data],\
        [i[6] for i in torque_data], [i[7] for i in torque_data], [i[8] for i in torque_data],\
        [i[9] for i in torque_data], [i[10] for i in torque_data], [i[11] for i in torque_data],\
        [i[12] for i in torque_data], [i[13] for i in torque_data], [i[14] for i in torque_data],\
        [i[15] for i in torque_data], [i[16] for i in torque_data], [i[17] for i in torque_data],\
        current, time_current]

    dataout = zip_longest(*d, fillvalue='')
    with open('/home/morf-one/GRAB/pos_data/result.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(("time_target", "out1", "out2", "error1","error2", "time_imu","imu_x","imu_y","imu_z","imu_w",\
                         "time_pos", \
                         "BC1", "CF1", "FT1", "BC2", "CF2", "FT2",\
                         "BC3", "CF3", "FT3", "BC4", "CF4", "FT4",\
                         "BC5", "CF5", "FT5", "BC6", "CF6", "FT6",\
                         "time_torque",\
                         "BC1_torque", "CF1_torque", "FT1_torque", "BC2_torque", "CF2_torque", "FT2_torque",\
                         "BC3_torque", "CF3_torque", "FT3_torque", "BC4_torque", "CF4_torque", "FT4_torque",\
                         "BC5_torque", "CF5_torque", "FT5_torque", "BC6_torque", "CF6_torque", "FT6_torque",\
                         "current", "time_current"))
        writer.writerows(dataout)

