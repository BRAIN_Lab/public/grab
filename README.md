# <center> GRAB: GRAdient-Based Shape-Adaptive Compliant Locomotion Control </center>

## Introduction

This study proposes a novel online-adaptive mechanism, namely "GRAB (GRAdient-Based Shape-Adaptive Compliant Locomotion Control)", that can adapt the shape of the driving signal via the combination of the gradient perturbation and the CPG attractor dynamics. We show that a simple loss function of tracking error reduction can be used to create compliant walking behaviours that comply to external perturbations to the robot. GRAB mechanism can be used to limit joint torque, which can help prevent motor failure, in a situation where heavy force is applied on the robot. Moreover, this mechanism can generally handle different types of loss function. This is particularly useful in the situation where we want to control the robot, such as to modulate the speed of a robot.

<div align="center">
<img src="https://gitlab.com/BRAIN_Lab/public/grab/-/raw/main/Fig.1.png" width="400">

**Fig 1:**  GRAB  controls  the  locomotion  of  the  legged  robot by  perturbing  the  dynamics  in  the  direction  that  minimises a loss function. At each step, the state is pulled by the force from  both  the  CPG  dynamical  surface  (shown  in  a  green arrow)  and  the  force  from  the  gradient  of  the  loss  (shownin  a  blue  arrow),  resulting  in  a  resultant  force  (shown  in  a red  arrow). 
</div>

## Method

In this work, we use a type of CPG called the SO(2)-network, whcih can be described as two coupled neurons. The activities of both neurons ($`a_1`$, $`a_2`$) and their outputs ($`o_1`$, $`o_2`$) are described by the following 
update equations:

```math
\begin{align}
\begin{bmatrix}
o_1 \\ o_2
\end{bmatrix}
\leftarrow \tanh 
\begin{bmatrix}
a_1 \\ a_2
\end{bmatrix} =
\tanh \begin{bmatrix}
w_{11}o_1 + w_{12}o_2 \\ w_{21}o_1 + w_{22}o_2
\end{bmatrix}
\end{align}
```

where the weight matrix for an SO(2) network depends on two variables $`\phi`$ and $`\alpha`$: 

```math
\begin{equation}
W = \begin{bmatrix}
w_{11} & w_{12} \\ w_{21} & w_{22}
\end{bmatrix} = \alpha \cdot
\begin{bmatrix}
cos(\phi) & sin(\phi)\\
-sin(\phi) & cos(\phi)
\end{bmatrix}
\end{equation} 
```

In order to incorporate adaptive behaviour into a CPG, we propose a novel adaptive CPG, namely GRAB, which incorporate the adaptive mechanism directly into the dynamics of the activity of the CPG's neurons.
We can implement GRAB as an additional update equation of the SO(2) dynamics described by the following update equations:. 

```math
\begin{equation}
\begin{bmatrix}
o_1\\
o_2
\end{bmatrix}
\leftarrow
\begin{bmatrix}
o_1\\
o_2
\end{bmatrix} - 
\begin{bmatrix}
\gamma_1\\
\gamma_2
\end{bmatrix}
\cdot
\begin{bmatrix}
\frac{\partial L}{\partial o_1}\\
\frac{\partial L}{\partial o_2}
\end{bmatrix}
\end{equation} 
```

where $`\gamma_1, \gamma_2`$ are hyper-parameters of the updating gains (or step-sizes). $`L`$ is a loss function, which is pre-defined. 
For example, $`L`$ can defined with a constraint such as a measure of mismatch between the desired behaviour and the current state of the robot.

<div align="center">
<img src="https://gitlab.com/BRAIN_Lab/public/grab/-/raw/main/Fig.2.png" width="400">

**Fig 2:**  CPG  system  with  GRAB  adaptive  mechanism  can be represented as a closed-loop system between the neurons and the robot system. As shown in the blue and red lines, the target signals from neurons are processed using the post-processing feature and used as a driving signal for the robot joints. The joint sensory error feedback will be monitored from the robot, as shown in the dashed purple line.The error feedback is then used to calculate the loss functionin order to changes the dynamics of the neurons.
</div>

### Tracking Error Reduction

Loss function is central to our method as it determines the desired compliant behaviour of the locomotion trajectory.
First, we explore a simple loss that attempt to reduce the tracking error, which is an important source of undesired motion in a robot's locomotion.
We define the tracking error as the square difference between the driving target position and the actual robot's joint position:

```math
\begin{equation}
L = \sum_{i=1}^{n}\frac{1}{2}(P_n-P_n')^2
\end{equation} 
```

where $`P_n`$ is the target position and $`P_n'`$ is the actual robot position. $`n`$ represents the $`n^{th}`$ driving joint of the robot.

### Speed Modulation
It is also possible to add multiple constraints to the trajectory dynamics. 
In our experiments, we investigate whether we can modulate the speed of the locomotion through GRAB mechanism. 
This is particularly useful when we want to perform a semi-automatic teleoperation, while 
still enable the robot to adapt to the external and internal perturbations. 

We define the speed modulation loss as the mismatch between our desired speed and the robot driving speed. In addition, we regularise the dynamics with the tracking-error reduction term. 

```math
\begin{equation}
 L =  \frac{1}{2}(V_{target}-V_{driving})^2 + \sum_{i=1}^{n}\frac{1}{2}(P_n-P_n')^2
\end{equation} 
```

where $`V_{driving} = k_{v} \phi`$ sets the oscillating frequency indirectly through an update dynamics:

```math
\begin{equation}
\begin{bmatrix}
o_1\\
o_2\\
\phi
\end{bmatrix}
\leftarrow
\begin{bmatrix}
o_1\\
o_2\\
\phi
\end{bmatrix} -
\begin{bmatrix}
\gamma_1\\
\gamma_2\\
\gamma_3
\end{bmatrix}
\cdot
\begin{bmatrix}
\frac{\partial L}{\partial o_1}\\
\frac{\partial L}{\partial o_2}\\
\frac{\partial L}{\partial o_2}
\end{bmatrix}
\end{equation}
```

### Bounding the Dynamics

An important property of SO(2) is the bounded periodic dynamics that is generated from its 
stable limit cycle property. By perturbing the signal with the gradient term, 
we run into the risk of driving the dynamics into an unstable region resulting in a divergence behaviour. 

To reconcile this problem, we must bound the perturbation to a certain value. 
We hypothesise that if the force acting on the particle is dominated by the dynamic force, $`|F_{dynamics}| > |F_{gradient}|`$,
then the trajectory stays bounded. 
GRAB uses the method of gradient clipping, where $`|\frac{\partial L}{\partial o}|`$ is capped below a maximum value
to avoid the gradient term becoming too large. 

For the speed modulation mechanism, we also has to create the bounded dynamics for $\phi$. 
To this end, we define the default dynamics of $`\phi`$ to be a point attractor dynamics pointing to a default value $`\phi_0`$,

```math
\begin{equation}
\phi \leftarrow \phi + k_{\phi}(\phi-\phi_0).
\end{equation} 
```

## The implementation of the project
### Required Installation 
- For real robot: Ubuntu 16.04 or later version.
- For simulation: v4_2_0 or the latest version of the CoppeliaSim. The CoppeliaSim can be download here https://www.coppeliarobotics.com/.

### Steps to run the simulation
There are 3 experiments according to the paper:
- Experiment 1: adaptation to an external weight
- Experiment 2: torque limitation
- Experiment 3: speed modulation and adaptation to external weight (light and heavy cubes, in Exp3_cubes.ttt scene file)

To run the simulation
- Start the desired CoppeliaSim scene
- Run the simulation by clicking the run button at the toolbox of the CoppeliaSim.

## Reference
GRAB: GRAdient-Based Shape-Adaptive Compliant Locomotion Control

If you have any questions about how to implement this method on your computer, robot you are welcome to raise issues and email to me. \
contact: sujet.ph@gmail.com
